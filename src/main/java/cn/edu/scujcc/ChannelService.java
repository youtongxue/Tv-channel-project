package cn.edu.scujcc;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChannelService {
	@Autowired
	private ChannelRepository repo;
	
	public boolean deleteChannel(String id) {
		boolean result = true;
		try {
		repo.deleteById(id);
		}catch (Exception e) {
		result = false;
		}
		return result;
	}
	

	
	
	public Channel getChannel(String id) {
		Channel result = null;
		if(null != id){
		if(repo.findById(id).isPresent()) {
			result = repo.findById(id).get();
			}
		}
		return result;
	}
	
	public List<Channel>getAllChannel(){
		List<Channel> result = null;
		result = repo.findAll();
		
		return result;
	}
	
	public Channel createChannel(Channel c){
		return repo.save(c);
	
	}


	public Channel updateChannle(Channel c) {
		Channel result = null;
		result = getChannel(c.getId());
		if (c.getName() != null) {
			result.setName(c.getName());
		}
		if (c.getDesc() != null) {
			result.setDesc(c.getDesc());
		}
		if (c.getUrl() != null) {
			result.setUrl(c.getUrl());
		}
		repo.save(result);
		return result;
	}
	
	public List<Channel> searchByDesc(String desc){
		return repo.findByDescLike(desc);
	}




	public List<Channel> searchByName(String desc) {
		return repo.findByDesc(desc);
	}
}
		/*
		Channel result = null;
		Optional<Channel> op = repo.findById(id);
		if(op.isPresent()){
		result = op.get();
		}
		return = op.get();
	}
}
*/