package cn.edu.scujcc;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChannelRepository extends MongoRepository<Channel,String>{
	public List<Channel> findByName(String n);
	public List<Channel> findByDesc(String d);
	public List<Channel> findByDescLike(String desc);
}
