package cn.edu.scujcc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
	@RequestMapping("/channel")
	
	public class ChannelController{
		
		@Autowired
		private ChannelService service;
		
//		@GetMapping
//		public List<Channel> searchByName(@PathVariable String desc){
//			List<Channel> result = null;
//			result = service.searchByName(desc);
//			return result;		
//		}
		
		
		@GetMapping("/desc/{desc}")
		public List<Channel> searchByDesc(@PathVariable String desc){
			List<Channel> result = null;
			result = service.searchByDesc(desc);
			return result;
		}
		
		@PutMapping
		public Channel updateChannel(@RequestBody Channel c) {
			Channel result = null;
			
			return result;
		}

		@PostMapping
		public Channel createChannel(@RequestBody Channel c) {
			Channel result = null;
			result = service.createChannel(c);
			return result;
		}
		
		
		
		
		@DeleteMapping("/{id}")
		public boolean deleteChannel(@PathVariable String id) {
			boolean result = false;
			result = service.deleteChannel(id);
			return result;
			
		}
		
		
		@GetMapping("/{id}")
			public Channel get(@PathVariable String id) {
				Channel result =  service.getChannel(id);
				return result;
			}
		
		@GetMapping
		public List<Channel> getAllChannels(){
			List<Channel> result = null;
			return result;
		}
	}

